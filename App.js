import React, {Component} from "react";
import { StyleSheet, TouchableOpacity, Text, View, Image, TextInput } from "react-native";
import AgeValidator from "./components/ageValidator/AgeValidator";
import MyList from "./components/myList/MyList";

export default class App extends Component{

    // Cofigo AgeValidator 

    /*constructor(props){
        super(props);
        this.state ={
            textValue: '',
            count: 0,
        };
    }

    changeTextInput = text => {
        this.setState({
            textValue: text
        });
    };

    onPress=()=>{
        this.setState({
            count: this.state.count + 1,
        })
    }

    render(){
        return(
            <View style={styles.container}>
                <View style={styles.text}>
                    <Text>Ingrese su edad</Text>
                </View>
                <AgeValidator/>
            </View>
        );
    }*/

    //Codigo MyList

    render(){
        return(
            <View style={styles.container}>
                <View style={styles.text}>
                    <Text> Redes Sociales </Text>
                </View>
                <MyList/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        paddingHorizontal: 10,
        backgroundColor: 'whitesmoke'
    },

    text: {
        alignItems: 'center',
        padding: 60,
    },
});