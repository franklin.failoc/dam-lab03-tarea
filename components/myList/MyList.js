import React, {Component} from "react";
import { StyleSheet, TouchableOpacity, Text, View, Image, FlatList } from "react-native";


export default class MyList extends Component{
    constructor(props){
        super(props)
        this.state = {
            lista: [
                {
                    nombre: 'Facebook',
                    img: 'https://cdn.icon-icons.com/icons2/2429/PNG/512/facebook_logo_icon_147291.png'

                },
                {
                    nombre: 'Instagram',
                    img: 'https://c0.klipartz.com/pngpicture/704/270/gratis-png-inicio-de-sesion-en-redes-sociales-instagram-ig-icono-de-instagram.png'

                },
                {
                    nombre: 'Whatsapp',
                    img: 'https://c0.klipartz.com/pngpicture/690/971/gratis-png-logotipo-de-whatsapp-logotipo-de-whatsapp-whatsapp-thumbnail.png'

                },
                {
                    nombre: 'Linkedin',
                    img: 'https://img2.freepng.es/20180320/kgq/kisspng-linkedin-logo-computer-icons-business-symbol-linkedin-icon-5ab176563be596.8497903315215796062453.jpg'
                }
            ]
        }
    }

    renderItem  = ({item}) =>(
        <TouchableOpacity>
            <View style={styles.itemContainer}>
                <Image style={styles.imagen} source={{uri: item.img}}/>

                <Text style={styles.itemName}>{item.nombre}</Text>
            </View>
        </TouchableOpacity>
    )

    keyExtractor = (item, index) => index.toString()

    FlatListSeparador = () => {
        return(
            <View style={styles.flat} />
        )
    }

    render(){
        return(
            <View style={styles.container}>
                <FlatList
                    keyExtractor={this.keyExtractor}
                    data={this.state.lista}
                    renderItem={this.renderItem}
                    ItemSeparatorComponent={this.FlatListSeparador} />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 20,
    },

    flat: {
        height: 1,
        width: '100%',
        backgroundColor: 'black'
    },

    imagen: {
        width: 100,
        height: 100,
        borderRadius: 50
    },

    itemContainer: {
        flex: 1,
        flexDirection: 'row',
        marginLeft: 20,
        justifyContent: 'flex-start',
        margin: 15,
    },

    itemName: {
        marginLeft: 20,
        fontSize: 20,
    },
})